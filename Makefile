.PHONY: all clean test mercury_unit

all: mercury_unit test

mercury_unit:
	${MAKE} -C lib mercury_unit


test:
	${MAKE} -C lib mercury_unit
	${MAKE} -C test test

clean:
	${MAKE} -C lib clean
	${MAKE} -C test clean

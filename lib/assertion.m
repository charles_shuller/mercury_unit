:- module assertion.
:- interface.
:- import_module univ.
:- import_module mercury_unit.test.

:- pred are_equal(univ::in, univ::in, mercury_unit.test.test_disposition::out) is det.
:- pred are_not_equal(univ::in, univ::in, mercury_unit.test.test_disposition::out) is det.


:- implementation.
:- import_module string.

are_equal(Expected, Actual, Disposition) :-
	Expected = Actual -> Disposition = pass
	;  Disposition = fail("Expected " ++ string(Expected) ++ ", but got " ++ string(Actual)).

are_not_equal(Expected, Actual, Disposition) :-
	Expected \= Actual -> Disposition = pass
	;  Disposition = fail("Expected anything but " ++ string(Expected) ++ ", but got " ++ string(Actual)).
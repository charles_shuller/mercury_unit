:- module test.
:- interface.
:- import_module list.
:- import_module int.

%%
%% Overview of terminology used
%%
%% Test -- Abstract entity
%% Test Script -- Concrete implementation of a test, it may be parameterized
%% Test Case -- Test Script bound up with it's parameters
%% Test Disposition -- If a test passed, failed, wasn't executed, etc...
%% Test Result -- Results of a test run, this includes not only the test disposition,
%%                Other useful information about the test run.  Duration, host executed on, etc...
%%


%%
%% The disposition type manages test dispositions.
%%
%% You may be quite happy to only use pass and fail, but larger test suites often require more than
%% this binary interpretation of test results.
%%
%% pass -- No bug was found
%% fail -- A bug was found
%%
%% unresolved -- Test requirs further investigation to further disposition it, i.e. you need to look at a printout
%% untested -- Test has not been run
%% unsupported -- Test can not be run on the current platform
%% xpass -- A test was expected to fail, but passed instead.  This is not always a good thing
%% xfail -- A test is expected to fail, i.e. the test is implemented before the feature
%% kpass -- A test which is known to fail has passed.  This is not always a good thing
%% kfail -- A test is known to fail, use this for bugs that won't be fixed anytime soon
%%
:- type test_disposition
	---> pass
        ;    fail(string)
	;    unresolved(string)
	;    untested
	;    unsupported
	;    xpass(string)
	;    xfail(string)
	;    kpass(string)
	;    kfail(string).


%%
%% A result potentially contains all sorts of usefull information,
%% though for our immediate needs, simply the test name and test disposition
%% are sufficient.
%%
:- type test_result
	---> test_result(result_test_name::string,
		         result_test_disposition::test_disposition).

%%
%% We don't yet support test script parameterization.  You can work around
%% this with currying when building the test case.
%%
:- type test_case
	---> test_case(test_name::string,
		       test_pred::pred(test_disposition)).
:- inst test_case == bound(test_case(ground,pred(out) is det)).


%%
%% A test suite is simply a group of test cases.
%%
:- type test_suite
	---> test_suite(test_suite_name::string,
		        test_list::list(test_case)).




%% Execute a single test. 
:- pred execute_test(test_case::in(test_case), test_result::out) is det.


%% Executes a list of tests.  Results are not printed as tests execute
:- pred execute_test_list(list(test_case)::in, list(test_result)::out, int::out, int::out) is det.




:- implementation.
:- import_module string.

execute_test(Test, Result) :-
	call(Test^test_pred, Disposition),
	Result = test_result(Test^test_name, Disposition).



:- pred execute_test_list_inner(test_case::in(test_case), list(test_result)::in, list(test_result)::out, int::in, int::out, int::in, int::out) is det.
execute_test_list_inner(Test, InResultList, OutResultList, InCount, OutCount, InPassCount, OutPassCount) :-
	execute_test(Test, Result),
	OutResultList = [Result | InResultList], OutCount = InCount + 1,
	(
	 if   Result^result_test_disposition = pass
	 then OutPassCount = InPassCount+1
	 else OutPassCount = InPassCount
	).


execute_test_list(TestList, ResultList, TestCount, PassCount) :-
	list.foldr3(execute_test_list_inner,		    
		    TestList,
		    [],
		    ResultList,
		    0,
		    TestCount,
		    0,
		    PassCount).
	
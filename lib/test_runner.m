:- module test_runner.
:- interface.
:- import_module io.
:- import_module int.
:- import_module float.
:- import_module list.
:- import_module mercury_unit.test.

%% This is the main entry point to this module, build up a test_suite list, and pass it in, then the results show up on
%% IO
%run_test_suites(TestSuiteList, !IO) is det
:- pred run_test_suites(list(test_suite)::in, io::di, io::uo) is det.


%run_tests(TestSuite, !IO) is det
:- pred run_tests(test_suite::in, io::di, io::uo) is det.

%run_tests(TestSuite, !TestCount, !PassCount, !IO) is det
:- pred run_tests(test_suite::in, int::in, int::out, int::in, int::out, io::di, io::uo) is det.

%pass_density(PassCount, TotalCount) = float is det.
:- func pass_density(int::in, int::in) = (float::out) is det.


:- implementation.
:- import_module string.


:- pred print_result(test_result::in, io::di, io::uo) is det.
print_result(Result, !IO) :-
	TestName = Result^result_test_name,
	(
	  Result^result_test_disposition = pass -> io.format("[%s] PASS\n", [s(TestName)], !IO)
	; Result^result_test_disposition = fail(Message) -> io.format("[%s] FAIL -- %s\n", [s(TestName), s(Message)], !IO)
	; Result^result_test_disposition = unresolved(Message) -> io.format("[%s] UNRESOLVED -- %s\n", [s(TestName), s(Message)], !IO)
	; Result^result_test_disposition = untested -> io.format("[%s] UNTESTED\n", [s(TestName)], !IO)
	; Result^result_test_disposition = unsupported -> io.format("[%s] UNSUPPORTED\n", [s(TestName)], !IO)	
	; Result^result_test_disposition = xpass(Message) -> io.format("[%s] XPASS -- %s\n", [s(TestName), s(Message)], !IO)
	; Result^result_test_disposition = xfail(Message) -> io.format("[%s] XFAIL -- %s\n", [s(TestName), s(Message)], !IO)
	; Result^result_test_disposition = kpass(Message) -> io.format("[%s] KPASS -- %s\n", [s(TestName), s(Message)], !IO)
	; Result^result_test_disposition = kfail(Message) -> io.format("[%s] KFAIL -- %s\n", [s(TestName), s(Message)], !IO)
	; io.write_string("Unsupported test disposition encountered\n", !IO)
	).


:- pred print_result_list(list(test_result)::in, io::di, io::uo) is det.
print_result_list([], !IO) :- true.
print_result_list([Result | ResultList], !IO) :-
	print_result(Result, !IO),
	print_result_list(ResultList, !IO).


:- pred print_result_summary(int::in, int::in, io::di, io::uo) is det.
print_result_summary(TestCount, PassCount, !IO) :-
	io.nl(!IO),
	io.write_string("Summary\n", !IO),
	io.write_string("--------------------------------------------------------------------------------\n", !IO),
	io.format("Test Count: %d\n", [i(TestCount)], !IO),
	io.format("Pass Count: %d\n", [i(PassCount)], !IO),
	io.format("Pass Density: %0.2f%%\n", [f(pass_density(PassCount, TestCount))], !IO).



run_tests(TestSuite, !TestCount, !PassCount, !IO) :-
	io.nl(!IO),
	io.nl(!IO),	
	io.format("%s\n", [s(TestSuite^test_suite_name)], !IO),
	io.write_string("================================================================================\n", !IO),
	execute_test_list(TestSuite^test_list, ResultList, ThisTestCount, ThisPassCount),
	!:TestCount = !.TestCount + ThisTestCount,
	!:PassCount = !.PassCount + ThisPassCount,
	print_result_list(ResultList, !IO),
	print_result_summary(ThisTestCount, ThisPassCount, !IO).

run_tests(TestSuite, !IO) :-
	run_tests(TestSuite, 0, _, 0, _, !IO).


run_test_suites(TestSuiteList, !IO) :-
	list.foldr3(run_tests,
		    TestSuiteList,
		    0,
		    TestCount,
		    0,
		    PassCount,
		    !.IO,
		    !:IO),
	io.nl(!IO),
	io.nl(!IO),
	io.write_string("Totals\n", !IO),
	io.write_string("================================================================================\n", !IO),	
	io.format("Total Test Count: %d\n", [i(TestCount)], !IO),
	io.format("Total Pass Count: %d\n", [i(PassCount)], !IO),
	io.format("Total Pass Density: %0.2f%%\n", [f(pass_density(PassCount, TestCount))], !IO),
	io.nl(!IO).

pass_density(PassCount, TestCount) = PassDensity :-
	PassDensity = (float(PassCount)/float(TestCount)) * 100.00.

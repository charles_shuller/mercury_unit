:- module assertion_tests.
:- interface.
:- import_module mercury_unit.
:- import_module mercury_unit.test.



:- func test_suite = test_suite is det.


:- implementation.
:- import_module int.
:- import_module univ.
:- import_module list.
:- import_module mercury_unit.assertion.

test_suite = TestSuite :-
	TestSuite = test_suite("AssertionTests",
			       [
				test_case("AreEqualPass",
					  (pred(Disposition::out) is det :- assertion.are_equal(univ(1), univ(1), Disposition))),
				
				test_case("AreEqualFail",
					  (pred(Disposition::out) is det :- assertion.are_equal(univ(1), univ(2), Disposition))),
				
				test_case("AreNotEqualPass",
					  (pred(Disposition::out) is det :- assertion.are_not_equal(univ(1), univ(2), Disposition))),
				
				test_case("AreNotEqualFail",
					  (pred(Disposition::out) is det :- assertion.are_not_equal(univ(1), univ(1), Disposition)))
			       ]).

:- module simple_tests.
:- interface.
:- import_module mercury_unit.
:- import_module mercury_unit.test.

:- func test_suite = test_suite is det.


:- implementation.
:- import_module list.

:- pred pass_test(test_disposition::out) is det.
pass_test(Disposition) :-
	Disposition = pass.

:- pred fail_test(test_disposition::out) is det.
fail_test(Disposition) :-
	Disposition = fail("Fail tests aways Fail").

:- pred unresolved_test(test_disposition::out) is det.
unresolved_test(Disposition) :-
	Disposition = unresolved("Unresolved tests are always unresolved").

:- pred untested_test(test_disposition::out) is det.
untested_test(Disposition) :-
	Disposition = untested.

:- pred unsupported_test(test_disposition::out) is det.
unsupported_test(Disposition) :-
	Disposition = unsupported.

:- pred xpass_test(test_disposition::out) is det.
xpass_test(Disposition) :-
	Disposition = xpass("Test covering feature X").

:- pred xfail_test(test_disposition::out) is det.
xfail_test(Disposition) :-
	Disposition = xfail("Prognosticator, see ticket http://ProgManagement.someorg.com/tickets/9999").

:- pred kpass_test(test_disposition::out) is det.
kpass_test(Disposition) :-
	Disposition = kpass("Bug 9889").

:- pred kfail_test(test_disposition::out) is det.
kfail_test(Disposition) :-
	Disposition = kfail("Bug 9889").


test_suite = TestSuite :-
	TestSuite = test_suite("Simple Tests",
			       [
				test_case("PassTest", pass_test),
				test_case("FailTest", fail_test),
				test_case("UnresolvedTest", unresolved_test),
				test_case("UntestedTest", untested_test),
				test_case("UnsupportedTest", unsupported_test),
				test_case("XPassTest", xpass_test),
				test_case("XFailTest", xfail_test),
				test_case("KPassTest", kpass_test),
				test_case("KFailTest", kfail_test),
				test_case("PredicateTest", (pred(Disposition::out) is det :- Disposition = pass))
			       ]).


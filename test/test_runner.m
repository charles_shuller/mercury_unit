:- module test_runner.
:- interface.
:- import_module io.


:- pred main(io::di, io::uo) is det.



:- implementation.
:- import_module list.
:- import_module mercury_unit.
:- import_module mercury_unit.test_runner.

:- import_module simple_tests.
:- import_module assertion_tests.


main(!IO) :-
	run_test_suites([
			 simple_tests.test_suite,
			 assertion_tests.test_suite
			],
			!IO).
